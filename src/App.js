import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import CVprojectAddition from './components/CVprojectAddition'
import CVprojects from './components/CVprojects'
import CVprojectUpdate from './components/CVprojectUpdate'
import LandingPage from './components/LandingPage'


// <Route path = "cv" element = {<CVprojects />}>
// </Route>

const App = () => {

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path = "/" element = {<LandingPage />} />
          <Route path = "/api">
              <Route path = "cv" element = {<CVprojects />} />
              <Route path = "cv/:projectId" element = {<CVprojectUpdate />} />  
              <Route path = "cv/add" element = {<CVprojectAddition />} />           
          </Route>
        </Routes>      
      </BrowserRouter>
    </>
  )
}


export default App