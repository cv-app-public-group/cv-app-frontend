import axios from "axios";


const baseUrl = "http://172.105.83.138:8080";
// const baseUrl = "http://localhost:8080";
const staticUri = "/api/cv";


const getAllprojects = () => {

    const url = baseUrl + staticUri;
        // console.log("URL: " + url);

    return axios.get(url);
}

const getProjectById = (id) => {

    const endpoint = staticUri + "/" + id

    const url = baseUrl + endpoint;

    return axios.get(url);
}

const createProject = (inputs) => {

    const url = baseUrl + staticUri;

    const config = {
        method: "post",
        url: url, 
        data: inputs,  
        headers: { "Content-Type": "application/json" }               
    };        

    return axios(config);
}

const updateProject = (id, inputs) => {

    const endpoint = staticUri + "/" + id

    const url = baseUrl + endpoint;

    const config = {
        method: "put",
        url: url, 
        data: inputs,  
        headers: { "Content-Type": "application/json" }               
    };        

    return axios(config)
}

const deleteProject = (id) => {

    const endpoint = staticUri + "/" + id

    const url = baseUrl + endpoint;

    const config = {
        method: "delete",
        url: url, 
        headers: { "Content-Type": "application/json" }               
    };   
    
    return axios(config);

}


export { getAllprojects, getProjectById, createProject, updateProject, deleteProject }