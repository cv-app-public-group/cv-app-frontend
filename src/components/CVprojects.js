import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { deleteProject, getAllprojects } from '../services/CVprojectsData'


const CVprojects = () => {

  const [projects, setProjects] = useState([])


  useEffect(() => {
    
    getAllprojects().then( response => {
        setProjects( () => response.data )
        console.log(response.data);
    } ).catch( error => {
        console.log(error);
    })
  
  }, [])


  const remove = async (projectId) => {

        const response = await deleteProject(projectId)

            console.log("Status after removing: " + response.status);

        getAllprojects().then( response => {
            setProjects( () => response.data );
        }).catch( error => {
            console.log(error);
        }) 
  }
  

  return (
    <div className = 'container'>
        <div className='d-grid justify-content-end' style={ {marginTop: '10px'} }>
            <Link to='/' className='btn btn-outline-secondary btn-sm'>
                <i className='bi-house'></i> Home
            </Link>
        </div>
        <h3 className = 'text-left'>CV projects</h3>
        <br />
        <Link to = "add" className = "btn btn-primary mb-2">Add project</Link>
        <br />
        <table className = 'table table-bordered table-striped'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>CV project</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    projects.map( project =>                         
                        <tr key = {project.id}>
                            <td> {project.id} </td>
                            <td> {project.firstName} </td>
                            <td> {project.lastName} </td>
                            <td> {project.projectName} </td>
                            <td> {project.createdAt} </td>
                            <td> 
                                {<Link to={`${project.id}`} className='btn btn-info'>Update</Link>} 
                                <button className='btn btn-danger'
                                        style={ {marginLeft: "10px"} }
                                        onClick={ (e) => remove(project.id) }>                          
                                    Delete
                                </button>                                        
                            </td>
                        </tr>
                    )
                }
            </tbody>
        </table>
    </div>
  )
}


export default CVprojects