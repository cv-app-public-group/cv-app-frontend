import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getProjectById } from '../services/CVprojectsData';


const CVproject = () => {

    // --- capturing dynamic segment --
    const params = useParams();

    const selectedId = params.projectId;

        console.log("dynamic segment value = " + selectedId);

    // --- setting initial state ---
    const [project, setProject] = useState({});
        
        console.log("initial project state: " + project);

    // --- making http request ---
    useEffect(() => {
      
        getProjectById(selectedId).then( response => {
            setProject( () => response.data );
            console.log("returned data: " + response.data);
        }).catch( error => {
            console.log("error fetching individual project");
        })

    }, [selectedId])
    

    return (
        <div className = 'container'>
            <br />
            <h3 className = 'text-left'>Selected CV project</h3>
            <br />
            <table className = 'table table-bordered table-striped'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>CV project</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                </thead>
                <tbody>
                    {                      
                        <tr key = {project.id}>
                            <td> {project.id} </td>
                            <td> {project.firstName} </td>
                            <td> {project.lastName} </td>
                            <td> {project.projectName} </td>
                            <td> {project.createdAt} </td>
                            <td> {project.updatedAt} </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
      )
}


export default CVproject