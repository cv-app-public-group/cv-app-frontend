import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { createProject } from '../services/CVprojectsData';


const CVprojectAddition = () => {

    const navigate = useNavigate();      // replaced useHistory()


    // --- handling state ---
    const initialState = {firstName: null, 
                          lastName: null, 
                          projectName: null};

    const [project, setProject] = useState(initialState);    


    // --- function for submitting form data ---
    const submitInputs = async (e) => {

        e.preventDefault();

        const response = await createProject(project);

        if(response.status === 200) {
            navigate("/api/cv");

        } else {
            console.log("Error posting form data: " + response.statusText);
        }       

    }


     return (
        <div className='container'>
            <br />
            <h3 className = 'text-left'>Add new project</h3>
            <br />
            <form onSubmit={submitInputs}>
                <div className='form-group'>
                    <label htmlFor='firstName'>First name</label>
                    <input type='text' id='firstName' className='form-control' 
                           onChange={ (e) => {
                            setProject( (prevState) => ({
                                            ...prevState,
                                            firstName: e.target.value}) )
                       }} />
                </div>
                <br />
                <div className='form-group'>
                    <label htmlFor='lastName'>Last name</label>
                    <input type='text' id='lastName' className='form-control'
                           onChange={ (e) => {
                            setProject( (prevState) => ({
                                            ...prevState,
                                            lastName: e.target.value}) )
                       }} />
                </div>
                <br />
                <div className='form-group'>
                    <label htmlFor='projectName'>CV project name</label>
                    <input type='text' id='projectName' className='form-control' 
                           onChange={ (e) => {
                            setProject( (prevState) => ({
                                            ...prevState,
                                            projectName: e.target.value}) )
                       }} />
                </div>
                <br />
                <button type='submit' className='btn btn-primary'>Submit</button>
                <Link to='/api/cv' className='btn'>Cancel</Link>
            </form>
        </div>
  )
}


export default CVprojectAddition