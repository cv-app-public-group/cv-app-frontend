import React from 'react'
import { Link } from 'react-router-dom';
import styledComponents from 'styled-components'
import { ReactComponent as ArchitectureDiagram } from '../cvapp-architecture.svg';
import { ReactComponent as DeploymentDiagram } from '../cvapp-deployment.svg';


const Div = styledComponents.p`
font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
font-size: large; 
`;

const CenteredStack = styledComponents.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;


const LandingPage = () => {
  return (
    <div className = 'container'>
      <Div>
        <p />
          <h1>Welcome, visitor!</h1>
        <br />
        <p style={ {color: 'rgb(105, 105, 103)', 'font-size': '12px'} }>
            <span style={ {'font-style': 'italic'} }>Author: </span> 
            František Kováč
        </p>
        <p className='landing-page'>
          I put this web site together for the purposes of taking interviews. This way I want to
          demonstrate my new skillset, i.e. building full stack web applications. 
          It is just a simple end-to-end solution enabling performing CRUD operations via REST API endpoints.            
        </p>
        <br />
        <p>
          In the future I want to primarily focus on Java programming language to become strong on the
          backend side.
        </p>
        <br />
        <p>
          The landing page has 2 parts:
        </p>
        <ul>
          <li>Diagrams - displaying a high-level architecture of the web app along with a deployment scenario</li>
          <li>Button   - for accessing the app</li>
        </ul>
        <br />
        <br />
        <ArchitectureDiagram />
        <br />
        <br />
        <DeploymentDiagram />
        <br />
        <br />
        <br />
        <br />
        <CenteredStack>
          <Link to={'/api/cv'} className = "btn btn-success mb-2">Launch app</Link>
          <a href='https://gitlab.com/cv-app-public-group'
             target={'_blank'}
             rel='noopener noreferrer'
             style={ {'font-size': '12px'} }>
               Source code
          </a>
        </CenteredStack>
      </Div>
    </div>
  )
}


export default LandingPage