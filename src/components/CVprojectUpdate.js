import React, { useState, useEffect } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom';
import { getProjectById, updateProject } from '../services/CVprojectsData';


    const CVprojectUpdate = () => {

        // --- handling redirection --
        const navigate = useNavigate();      // replaced useHistory()


        // --- capturing dynamic segment --
        const { projectId } = useParams();

            console.log("dynamic segment value = " + projectId);


        // --- handling state ---
        const [project, setProject] = useState({});

        
        useEffect(() => {
            // making http request 
            getProjectById(projectId).then( response => {
                setProject( () => response.data );
                console.log("returned data: " + response.data);
            }).catch( error => {
                console.log("error fetching individual project");
            })

        }, [projectId])
    


    // --- function for submitting form data ---
    const submitInputs = async (e) => {

        e.preventDefault();

        const response = await updateProject(projectId, project);

        if(response.status === 200) {
            navigate("/api/cv");

        } else {
            console.log("Error posting form data: " + response.statusText);
        }       

    }


     return (
        <div className='container'>
            <br />
            <h3 className = 'text-center'>Update project</h3>
            <br />
            <form onSubmit={submitInputs}>
                <div className='form-group'>
                    <label htmlFor='firstName'>First name</label>
                    <input type='text' id='firstName' className='form-control' 
                           value={project.firstName}
                           onChange={ (e) => {
                                setProject( (prevState) => ({
                                                ...prevState,
                                                firstName: e.target.value}) )
                           }} />
                </div>
                <br />
                <div className='form-group'>
                    <label htmlFor='lastName'>Last name</label>
                    <input type='text' id='lastName' className='form-control'
                           value={project.lastName}
                           onChange={ (e) => {
                                setProject( (prevState) => ({
                                                ...prevState,
                                                lastName: e.target.value}) )
                           }} />
                </div>
                <br />
                <div className='form-group'>
                    <label htmlFor='projectName'>CV project name</label>
                    <input type='text' id='projectName' className='form-control' 
                           value={project.projectName}
                           onChange={ (e) => {
                                setProject( (prevState) => ({
                                                ...prevState,
                                                projectName: e.target.value}) )
                           }} />
                </div>
                <br />
                <button type='submit' className='btn btn-primary'>Submit</button>
                <Link to='/api/cv' className='btn'>Cancel</Link>
            </form>
        </div>
  )
}


export default CVprojectUpdate